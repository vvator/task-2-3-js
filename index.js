/**
* Runner JavaScript Task2/3 
* @copyright V.A.Vikhliayeu 2016 
*/
'use strict';
(function() {

    var functionModule = require('./js/functions');
    var STKit = require('./js/stkit');
    var log = console.log.bind();
    log('-----------------------------------');
    log('MEMOIZER:');
    log('-----------------------------------');
    var momoizer = STKit.memoizer(functionModule.sum);
    log(momoizer(500, 50, 5, 1));
    log(momoizer(500, 50, 5, 1));
    log(momoizer(500, 50, 5, 1));
    log(momoizer(500, 50, 5, 1));
    log(momoizer(500, 50));
    log(momoizer(500, 50, 5, 1));
    log(momoizer(500, 50, 5, 1));
    log(momoizer(500, 50, 5, 1, 2));
    log(momoizer(500, 50, 5, 1));
    log(momoizer(500, 50, 5, 1, 2));
    log('\n\n');

    log('-----------------------------------');    
    log('IS ARRAY LIKE OBJECT CHECKER:');
    log('-----------------------------------');
    var testALO_1 = { 10: 100, 20: 'test', 1: 'test2', length: 3, 3: 'test3' };
    var testALO_2 = ['test_0', 'test_1', 'test_2', 'test_3'];
    var testALO_3 = { '1': 'a', '5': 'b', '2': 'c', length: '2a' };
    var testALO_4 = 'jdskhf';
    var testALO_5 = { name: 'test', size: 2, length: 5 };
    log(STKit.isArrayLikeObject(testALO_1) + ' <== isArrayLike <== ' + testALO_1);
    log(STKit.isArrayLikeObject(testALO_2) + ' <== isArrayLike <== ' + testALO_2);
    log(STKit.isArrayLikeObject(testALO_3) + ' <== isArrayLike <== ' + testALO_3);
    log(STKit.isArrayLikeObject(testALO_4) + ' <== isArrayLike <== ' + testALO_4);
    log(STKit.isArrayLikeObject(testALO_5) + ' <== isArrayLike <== ' + testALO_5);
    log('\n\n');

    log('-----------------------------------');
    log('DEBEHAVIORIZER:');
    log('-----------------------------------');
    var objDeb = {
        field1: 100500,
        method1: functionModule.sum,
        method2: functionModule.factorial,
        arr: ['arg', 2, [1, 2, 3]]
    };
    var stateObj = STKit.debehaviorizer(objDeb);
    log('Object => ');
    log(objDeb);
    log();
    log(STKit.debehaviorizer(objDeb, true));
    log();
    log(objDeb);
    log();
    log(stateObj);
    log('\n\n');

    log('-----------------------------------');
    log('SemiColonSON:');
    log('-----------------------------------');
    var data = 'a,k;alertMethod,|console.log("\tHappy New Year!");console.log("\tHoHoHOoooo!!!");|;methodCalculate,|function(a,j,i){return a+1+j*i}|;methodTRUE,|return true|;key,value;methodFALSE,|return false|;key1,value;method2,|return true|';
    log('dataString: "' + data+'"');
    var semiColonSON = STKit.semiColonSON();
    var objectBuild = semiColonSON.parseSemiColon(data);
    log('objectBuild = ');
    log(objectBuild);
    log("\nTest run method alertMethod: ");
    objectBuild.alertMethod();
    log("Test run method methodCalculate: " + objectBuild.methodCalculate(2, 3, 5));
    log("Test run method methodTRUE: " + objectBuild.methodTRUE());
    log("Test run method methodFALSE: " + objectBuild.methodFALSE());
    log("\n");
    log('---');

    log("empty data");    
    data = '';
    log('dataString: "' + data+'"');
    semiColonSON = STKit.semiColonSON();
    objectBuild = semiColonSON.parseSemiColon(data);
    log('objectBuild = ');
    log(objectBuild);
    log("\n");
    log('---');

    data = ';;;;key,value;key1,value1;key2,value2;key2,value;';
    log('dataString: "' + data+'"');
    semiColonSON = STKit.semiColonSON();
    objectBuild = semiColonSON.parseSemiColon(data);
    log('objectBuild = ');
    log(objectBuild);
    log("\n");
    log('---');

    data = 'a,k;alertMethodWithLogic,|function(firstMessage){if(firstMessage){console.log("\tHappy New Year!");}else{console.log("\tHoHoHOoooo!!!");}}|;methodParamReturner,|function(a){return a}|;methodTRUE,|return true|;key,value;methodFALSE,|return false|;key1,value;method2,|return true|';
    log('dataString: "' + data+'"');
    semiColonSON = STKit.semiColonSON();
     objectBuild = semiColonSON.parseSemiColon(data);
    log('objectBuild = ');
    log(objectBuild);
    log("\nTest run method alertMethodWithLogic(true): ");
    objectBuild.alertMethodWithLogic(true);
    log("Test run method alertMethodWithLogic(false): ");
    objectBuild.alertMethodWithLogic(false);
    log("Test run method methodCalculate: " + objectBuild.methodParamReturner(100500));
    log("Test run method methodTRUE: " + objectBuild.methodTRUE());
    log("Test run method methodFALSE: " + objectBuild.methodFALSE());
    log("\n");
    log('---');

    data = 'key,value;key1,value1;key2,value2;key2,value;method1,|function(a,j,i){return a+1}|;method2,|return true|;arrayField:k1,v1;k2,v2;k3,v3;;;';
    log('dataString: "' + data+'"');
    semiColonSON = STKit.semiColonSON();
    objectBuild = semiColonSON.parseSemiColon(data);
    log('objectBuild = ');
    log(objectBuild);
    log("\n");
    log('---');

    data = 'method1,|function(a,j){return a+j}|;method2,|return true|;arrayField:arrK1,arrV1;arrK2,arrV2;arrK3,arrV3;arrK4,arrV4;arrK5,arrV5';
    log('dataString: "' + data+'"');
    semiColonSON = STKit.semiColonSON();
    objectBuild = semiColonSON.parseSemiColon(data);
    log('objectBuild = ');
    log(objectBuild);

    log("\nTest run method method1: " + objectBuild.method1(10,5));
    log("Test run method method2: " + objectBuild.method2());
})();
