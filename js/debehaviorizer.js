/**
* @copyright V.A.Vikhliayeu 2016 
* @module Debehaviorizer
*/
'use strict';
var log = console.log.bind();

var debehaviorizer = function(obj, key) {
    if (key === true) {
        var methods = [];
        for (var p in obj) {
            if (typeof obj[p] === 'function') {
                try {
                    methods.push(obj[p]);
                    delete obj[p];
                } catch (ex) {
                    log('[WARN]: ' + ex);
                }
            }
        }
        return methods;
    } else {
        var newObject = {};
        for (var pN in obj) {
            if (typeof obj[pN] !== 'function') {
                newObject[pN] = obj[pN];
            }
        }
        return newObject;
    }
};

module.exports = debehaviorizer;
