/**
* @copyright V.A.Vikhliayeu 2016 
* @module FunctionModule
*/
'use strict';
var functionModule = (function() {
    /**
     *factorial.
     *This function  return factorial for number.
     *
     *@throws {Error} throw error will thrown if argument is not number type.
     *@private
     *@param {number} number for calculate factorial.
     *@returns {number} Returns the number (calculated result function).
     */
    function factorial(n) {
        if (typeof n !== 'number') {
            throw 'Error: incorrect argument';
        }
        var res = 1;
        while (n !== 1) {
            res *= n--;
        }
        return res;
    }

    /**
     *sum.
     *This function  return sum for numbers from arguments.
     *
     *@private
     *@param {arguments} numbers for calculate sum.
     *@returns {number} Returns the number (calculated result sum).
     */
    function sum() {
        var tmpSum = 0;
        for (var i in arguments) {
            if (typeof arguments[i] === 'number') {
                tmpSum += arguments[i];
            }
        }
        console.log(tmpSum);
        return tmpSum;
    }

    /**
     *Returns function from this module.
     *
     *@public
     *@returns {Function}:
     *factorial,
     *sum     
     */
    return {
        /**
         *Function for calculate factorial.
         *@param {number}
         */
        factorial: factorial,
        /**
         *Function calculate summ.
         *@param {numbers}
         */
        sum: sum,
    };

})();

module.exports = functionModule;
