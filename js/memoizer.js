/**
* @copyright V.A.Vikhliayeu 2016 
* @module memoizer
*/
'use strict';
var memoizer = function(func) {
    /** @private */
    var cache = {};
    /** @private */
    var slice = Array.prototype.slice;
    /** @private */
    var arr = slice.call(arguments);

    /**
     * getResultObj.
     * This function  return result for function.
     * If function result exist in history (cache) then been return result from cashe,
     * else result been calculate for function/
     *    
     * @private    
     * @return {number} Returns calculated result function.
     */
    function getResultObj() {
        var arr = slice.call(arguments);
        if (!(func in cache)) {
            cache[func] = cache[func] || {};
            cache[func][''] = func;
        }
        if (arr in cache[func]) {
            console.log('\tFunction result is getting from cache');
        } else {
            console.log('Calculate function result');
            if (arr.length > 0) {
                cache[func][arr] = func.apply(this, arr);
            } else {
                cache[func][arr] = func;
            }
        }
        return cache[func][arr];
    }

    /** @constructs 
     * @throws {Exception} throw error will thrown if argument is not a function type.
     */
    return (function() {
        if (typeof func !== 'function' || arr.length === 0) {
            throw 'Exception: incorrect argument';
        } else {
            if (arr.length > 1) {
                arr = arr.slice(1);
                getResultObj.apply(this, arr);
            }
        }
        return getResultObj;
    })();
};


module.exports = memoizer;
