/**
* @copyright V.A.Vikhliayeu 2016 
* @module semiColon
*/
'use strict';
var semiColon = function() {
    /** @private */
    var objectBuild = {};

    /**
     * parseFunct.
     * This function  return object type Function after parsing  
     * string parameter data.
     *
     * @private  
     * @param {strind}  data - SemiColonString
     * @return {object} Returns object base data from parseSemiColon string.
     */
     function parseFunct(strFunction) {
        var f;
        if (strFunction.match(/function/)) {
            f = new Function('return ' + strFunction)();
        } else {
            f = new Function(strFunction);
        }
        return f;
    }

    /**
     * parseSemiColon.
     * This function  return object after parsing parameter data.
     *
     * @throws {Error}  error will thrown if object is not created.
     * @private  
     * @param {strind}  data - SemiColonString
     * @return {object} Returns object base data from parseSemiColon string.
     */
    var addFunctionsToObject = function(dataFunction) {
        if (dataFunction !== null) {
            for (var f in dataFunction) {
                var keyval = dataFunction[f].split(',|');
                keyval[1] = keyval[1].replace(new RegExp('[|]$'), '');
                objectBuild[keyval[0]] = parseFunct(keyval[1]);
            }
        }
    };

    /**
     * parseSemiColon.
     * This function  return object after parsing parameter data.
     *
     * @throws {Error}  error will thrown if object is not created.
     * @private  
     * @param {strind}  data - SemiColonString
     * @return {object} Returns object base data from parseSemiColon string.
     */
    function parseSemiColon(data) {
        if (data !== '') {
            data = data.replace(new RegExp('^;*'), '');
            data = data.replace(new RegExp(';*$'), '');
            data = data.replace(new RegExp(/^[;]*/), '');
            var dataARR = data.match(new RegExp('([A-Z,a-z,0-9]*?[:](.*)*?)$', 'g'));
            data = data.replace(new RegExp('(;[A-Z,a-z,0-9]*?[:](.*)*?)$', 'g'), '');
            var dataFUNC = data.match(new RegExp('([A-Z,a-z,0-9]*?,[|].*?[|])', 'g'));
            data = data.replace(new RegExp('([A-Z,a-z,0-9]*?,[|].*?[|];)', 'g'), '');
            data = data.replace(new RegExp(',', 'g'), '":"');
            data = data.replace(new RegExp(';', 'g'), '","');
            data = '{"' + data + '"}';
            try {
                objectBuild = JSON.parse(data);

                addFunctionsToObject(dataFUNC);

                if (dataARR !== null) {
                    var arrKV = dataARR.join().split(":");
                    var arrObj = arrKV[1].split(';');
                    objectBuild[arrKV[0]] = [];
                    for (var ii in arrObj) {
                        var arOb = {};
                        var arrElement = arrObj[ii].split(',');
                        arOb[arrElement[0]] = arrElement[1];
                        objectBuild[arrKV[0]].push(arOb);
                    }
                }
            } catch (e) {
                throw 'Exception: object is not created';
            }
        }
        return objectBuild;
    }

    /**
     *Returns function from this module.
     *
     *@public
     *@returns {Function}:
     *parseSemiColon
     */
    return {
        /**
         *Function for parsing SemiColonSON string to object.
         *@param {string}
         */
        parseSemiColon: parseSemiColon,
    };
};

module.exports = semiColon;
