/**
* @copyright V.A.Vikhliayeu 2016 
* @module STKit
*/
'use strict';
var STKit = (function() {
    /**
     * isArrayLikeObject.
     * This function  return true if object is ArrayLike.
     *
     * @private
     * @param data - check ArrayLikeObject.
     * @returns {boolean} Returns the true if object is ArrayLike, 
     * or false if param data is not ArrayLike object.
     */
    function isArrayLikeObject(data) {
        function isObject(obj) {
            return obj !== null && typeof obj === 'object';
        }

        function hasOwnPropertyLength(obj) {
            return obj.hasOwnProperty('length') && typeof obj.length === 'number';
        }

        if ((isObject(data) && hasOwnPropertyLength(data))) {
            for (var item in data) {
                if (item !== 'length' && isNaN(parseInt(item))) {
                    return false;
                }
            }
        } else {
            return false;
        }
        return true;
    }    
    return {
        /**
        * Module with Memoization functions.
        * @param {function}
        */
        memoizer: require('./memoizer'),
        /**
        * Module with Debehaviorize functions.
        * @param {object}
        * @param {boolean}
        */   
        debehaviorizer: require('./debehaviorizer'),  
        /**
        * Module with SemiColonSON functions.
        * @param {string}
        */     
        semiColonSON: require('./semicolonson'),
        /**
        * Function for check array-like object.
        * @param {object}
        */
        isArrayLikeObject: isArrayLikeObject
    };
})();

module.exports = STKit;
